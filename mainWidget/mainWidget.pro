#-------------------------------------------------
#
# Project created by QtCreator 2015-01-26T20:08:31
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = mainWidget
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    logindlg.cpp

HEADERS  += widget.h \
    logindlg.h

FORMS    += widget.ui \
    logindlg.ui
