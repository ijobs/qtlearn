#include "logindlg.h"
#include "ui_logindlg.h"
#include <QMessageBox>

loginDlg::loginDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::loginDlg)
{
    ui->setupUi(this);
    ui->pwdLineEdit->setEchoMode(QLineEdit::Password);
}

loginDlg::~loginDlg()
{
    delete ui;
}

void loginDlg::on_loginBtn_clicked()
{
    QString userName = ui->userLineEdit->text().trimmed();
    QString passwd = ui->pwdLineEdit->text();
    if (userName == "root" && passwd == "1234")
    {
        accept();
    }
    else
    {
        QMessageBox::warning(this, "Warning", "您输入的用户名或密码有误！", QMessageBox::Yes);
        ui->userLineEdit->clear();
        ui->pwdLineEdit->clear();
        ui->userLineEdit->setFocus();
    }
}
