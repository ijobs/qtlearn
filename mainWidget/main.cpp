#include "widget.h"
#include "logindlg.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    loginDlg d;
    Widget w;
    if (d.exec() == QDialog::Accepted)
    {
         w.show();
         return a.exec();
    }
    else
    {
        return 0;
    }
}
